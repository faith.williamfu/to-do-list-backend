package com.tw.todoitems.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

class ConnectorTest {

    private static Connector connector;

    @BeforeAll
    static void init() {
        connector = new Connector();
    }

    @Test
    void should_get_db_config_info() {
        Config config = connector.getConfig();

        Assertions.assertNotNull(config);
        Assertions.assertEquals("jdbc:mysql://localhost:3306/student_management_system", config.getDatabaseURL());
        Assertions.assertEquals("root", config.getUsername());
        Assertions.assertEquals("123456", config.getPassword());
    }

    @Test
    void should_get_connect() throws SQLException {
        Connection connection = connector.createConnect();

        Assertions.assertNotNull(connection);
    }

}