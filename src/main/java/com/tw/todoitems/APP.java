package com.tw.todoitems;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;
import com.tw.todoitems.service.ToDoItems;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
public class APP {

    @Autowired
    private ToDoItems toDoItems;

    public static void main(String[] args) {
        SpringApplication.run(APP.class, args);
    }

    @GetMapping("/items")
    @CrossOrigin(origins = "*")
    public List<Item> getAllItems() {
        // Need to be implemented
        return null;
    }

    @PostMapping("/items")
    @CrossOrigin(origins = "*")
    public Item addItem(@RequestBody Item item) {
        // Need to be implemented
        return null;
    }

    @PutMapping("/items")
    @CrossOrigin(origins = "*")
    public boolean updateItem(@RequestBody Item item) {
        // Need to be implemented
        return false;
    }

    @DeleteMapping("/items/{id}")
    @CrossOrigin(origins = "*")
    public void deleteItem(@PathVariable int id) {
        // Need to be implemented
    }
}
