package com.tw.todoitems.model;

public class Item {
    private int id;
    private String text;
    private ItemStatus status;

    public Item(String text) {
        this.text = text;
    }

    public Item(int id, String text, ItemStatus status) {
        this.id = id;
        this.text = text;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }
}
