package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;

import java.util.List;

public class ItemRepo {
    private Connector connector;

    public ItemRepo() {
        this.connector = new Connector();
    }

    public Item addItem(Item item) {
        // Need to be implemented
        return null;
    }

    public List<Item> findItems() {
        // Need to be implemented
        return null;
    }

    public boolean updateItem(Item item) {
        // Need to be implemented
        return false;
    }

    public boolean deleteItem(int id) {
        // Need to be implemented
        return false;
    }

}
